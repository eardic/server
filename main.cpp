#include <QtCore/QCoreApplication>
#include "Server.h"
#include <string>
#include <iostream>
using namespace std;
using namespace NetworkArdic;


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    Server ser;

    cout << "Ready\n" << "Waiting for client...\n";

    ser.waitForNewConnection(-1);

    for(int i=0 ; i < 100 ; ++i){
        ser.SendData(QString( QChar(i) ));
    }

    return a.exec();
}
