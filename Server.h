/*

Created BY : EMRE ARDI�
Creation DATE : 26/10/2012

Server interface
Default port is 4000

*/

#ifndef SERVER_H
#define SERVER_H

#include <QtNetwork>
#include <QObject>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

namespace NetworkArdic
{

    class Server: public QTcpServer
    {

        Q_OBJECT
        public:

          Server(QObject * parent = 0 , quint16 port = 4000);
          virtual  ~Server();

          // This is the function which sends data to client.
          void SendData(QString data);

        private slots:

          // When new connection is made, this function is called.
          // The function prints client information.
          void acceptConnection();

          // Reads data from clients.
          void startRead();

          // Called when client disconnects.Prints client info.
          void disconnected();

        private:

          QTcpSocket * client;

    };

}

#endif // SERVER_H
