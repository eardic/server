/*

Created BY : EMRE ARDI�
Creation DATE : 26/10/2012

Server source file

*/


#include "Server.h"
#include <string>
#include <iostream>
using namespace std;

namespace NetworkArdic{

    Server::Server(QObject* parent , quint16 port): QTcpServer(parent)
    {
      connect(this, SIGNAL(newConnection()),this, SLOT(acceptConnection()));
      client = NULL;
      listen(QHostAddress::Any, port );
    }

    Server::~Server()
    {
      delete client;
      close();
    }

    void Server::acceptConnection()
    {
      client = nextPendingConnection();

      connect(client, SIGNAL(readyRead()), this, SLOT(startRead()));
      connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));

      qDebug() << "New client from:" << client->peerAddress().toString();

    }

    void Server::startRead()
    {
        while(client->canReadLine())
        {
            QString line = QString::fromUtf8(client->readLine()).trimmed();
            qDebug() << "Client :" << line;
        }

    }

    // Before this function to be used, waitForNewConnection
    // function should be used with -1.
    // Send "data" to remote computer.
    void Server::SendData(QString data)
    {
        // Only send the text to the client if it's not empty:
        if(client != NULL && client->isWritable())
        {
            client->write(QString(data + "\n").toUtf8());
            client->waitForBytesWritten();
            client->flush();
        }
        else
        {
            qDebug() << "Client not found or not ready.\n";
        }

    }

    void Server::disconnected()
    {
        qDebug() << "Client disconnected:" << client->peerAddress().toString();
    }

}
