#-------------------------------------------------
#
# Project created by QtCreator 2012-10-26T00:51:07
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = Server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Server.cpp

HEADERS += \
    Server.h
